#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__init__.py file of {{ cookiecutter.project_slug }} package.

Allows you to treat a directory as if it was a python module.
Imports all parts from {{ cookiecutter.project_slug }} here.

More info:
https://docs.python.org/3/tutorial/modules.html
http://timothybramlett.com/How_to_create_a_Python_Package_with___init__py.html
https://talkpython.fm/episodes/show/12/deep-dive-into-modules-and-packages
"""

from ._version import __version__ # noqa

# import everything from submodules for tests.
from .{{ cookiecutter.project_slug }} import {{ (cookiecutter.project_slug.title()).replace('_', '') }}  # noqa
from .exceptions import Error  # noqa
