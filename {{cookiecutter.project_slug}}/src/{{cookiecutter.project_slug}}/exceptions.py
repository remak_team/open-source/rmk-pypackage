#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: exceptions.py
"""
Main module Exceptions file.

Put your exception classes here.

https://julien.danjou.info/blog/2016/python-exceptions-guide
"""


class Error(Exception):  # pragma: no cover
    """Basic exception for errors raised by something."""
