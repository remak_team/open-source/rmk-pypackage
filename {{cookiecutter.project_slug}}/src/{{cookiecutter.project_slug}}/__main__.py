#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
__main__.py file of {{ cookiecutter.project_slug }} package.

__main__.py is run as '__main__' when you run a package as the main program.
For instance, python -m {{ cookiecutter.project_slug }}
"""

import logging

from .{{ cookiecutter.project_slug }} import {{ (cookiecutter.project_slug.title()).replace('_', '') }}  # noqa


logger = logging.getLogger(__name__)


def main():  # pragma: no cover
    """CLI console script entry point."""
    logging.basicConfig(level=logging.DEBUG)
    logger.info('Hello from {{ cookiecutter.project_slug }}!')
    {{(cookiecutter.project_slug.title()).replace('_', '')}}.simple_add(2, 3)

    example_instance = {{(cookiecutter.project_slug.title()).replace('_', '')}}()
    example_instance.method()


if __name__ == '__main__':
    main()
