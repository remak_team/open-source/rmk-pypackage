#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module determining the whole distribution version."""

import logging


logger = logging.getLogger(__name__)

try:
    import pkg_resources
    __version__ = pkg_resources.require('{{ cookiecutter.project_slug }}')[0].version
except Exception:  # pylint: disable=W0703
    __version__ = 'unknown'
    logger.warning('Unable to read version from git repository.')
