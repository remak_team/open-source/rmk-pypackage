======================
Using this environment
======================

Prerequisites
^^^^^^^^^^^^^

* Python3.6+

* Git_

* Pip


Installation
^^^^^^^^^^^^
1. Prepare (virtual)environment::

    pip install pip wheel -U
    pip install pre-commit
    pre-commit install

* All requirements can be found in ``setup.cfg``.

* Prod requirements are reused in both Dev and Test requirements.

Using the environment
^^^^^^^^^^^^^^^^^^^^^

* List make commands with ``make help``.

* Install requirements with ``pip install -e .[dev,lint,docs]``

* Testing with pytest: ``python setup.py test``

* Every commit is linted with `pre-commit`_, you can run it manually: ``pre-commit run --all-files``

Versioning guide
^^^^^^^^^^^^^^^^

The project is using setuptools_scm_ for automatic versioning according to SemVer_ specification.
How to use setuptools_scm_ with Git_ tags https://github.com/pypa/setuptools_scm#default-versioning-scheme

Tools used
----------

Python
^^^^^^

* Python_ - The mother of all.

* Cookiecutter_ - to create this template.

* Sphinx_ - Sphinx is a tool that makes it easy to create intelligent and beautiful documentation, check rst specs.

* PyTest_ - The pytest framework makes it easy to write small tests, yet scales to support complex functional testing \
  for applications and libraries.

* pytest-runner_ - ``python setup.py test`` support for pytest runner

* `pre-commit`_ - A framework for managing and maintaining multi-language pre-commit hooks.

* Pylint_ - is a tool that checks for errors in Python code, tries to enforce a coding standard and looks for code smells.

* setuptools_scm_ - handles managing your python package versions in scm metadata instead of declaring them as the version argument or in a scm managed file.

* PythonPackaging_ - Python Packaging User Guide.

* Virtualenv_ - is a tool to create isolated Python environments.

* `Virtualenv Wrapper`_ - is a set of extensions to Ian Bicking’s virtualenv tool.

* setuptools-git_ - plugin for setuptools that enables git integration. Once installed, Setuptools can be told to include in a package \
  distribution all the files tracked by git. This is an alternative to explicit inclusion specifications with MANIFEST.in

Source code versioning
^^^^^^^^^^^^^^^^^^^^^^

* Git_ - Git is a free and open source distributed version control system designed to handle everything from small to \
  very large projects with speed and efficiency.

* GitLab_ - The platform for modern developers.

Continuous integration / Continuous development (CI/CD)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* `GitLab CI`_ - GitLab has integrated CI/CD pipelines to build, test, deploy, and monitor your code.

Other technologies/tools
------------------------

* Bash_ - Bash is the shell, or command language interpreter, for the GNU operating system.

* Make_ - GNU Make is a tool which controls the generation of executables and other non-source files of a program from \
  the program's source files.

* YAML_ - YAML is a human friendly data serialization standard for all programming languages.

* JSON_ - lightweight data-interchange format.

* reStructuredText_ - "reStructuredText" is ONE word, not two!

Project directory structure
---------------------------

.. code:: bash

        {{ cookiecutter.project_slug }}

* `{{ cookiecutter.project_slug }}` - Project root directory.

.. code:: bash

        ├── docs
        │   ├── _source
        │   │   ├── human_docs
        │   │   │   ├── human_doc1.rst
        │   │   │   └── human_doc1.rst
        │   │   ├── authors.rst
        │   │   ├── conf.py
        │   │   ├── contributing.rst
        │   │   ├── changelog.rst
        │   │   ├── index.rst
        │   │   ├── installation.rst
        │   │   ├── readme.rst
        │   │   └── usage.rst
        │   ├── make.bat
        │   └── Makefile

* **docs** - Directory containing everything about documentation - more info Sphinx_.

* **human_docs** - is directory supposed to contain docs written by author, \
                ``apidoc`` generated docs is stored in ``_source`` directory

.. code:: bash

        ├── src
        │   └── {{ cookiecutter.project_slug }}
        │       ├── __init__.py
        │       ├── _version.py
        │       ├── {{ cookiecutter.project_slug }}.py
        │       └── {{ cookiecutter.project_slug }}_exceptions.py

* **src** - Directory contains all source code written by the author.

* **{{ cookiecutter.project_slug }}** - Directory with code of the package.
* **_version.py** - handles project version using setuptools_scm_

.. code:: bash

        ├── tests
        │   ├── conftest.py
        │   └── test_{{ cookiecutter.project_slug }}.py


* **test** - Directory containing all of the application tests, prepend `test_` for every test file, \
            more info PyTest_.

.. code:: bash

        ├── AUTHORS.rst
        ├── CONTRIBUTING.rst
        ├── .coveragerc
        ├── .editorconfig
        ├── .gitattributes
        ├── .gitignore
        ├── .gitlab-ci.yml
        ├── CHANGELOG.rst
        ├── LICENSE
        ├── Makefile
        ├── .pre-commit-config.yaml
        ├── README.rst
        ├── requirements.txt
        ├── setup.cfg
        └── setup.py

* **AUTHORS.rst** - Write about authors of the project.

* **CONTRIBUTING.rst** - How to contribute to the project.

* **.coveragerc** - Configuration file for coverage report, more info Coverage_.

* **.editorconfig** - More info at http://editorconfig.org.

* **.gitattributes** - More info at https://git-scm.com/docs/gitattributes

* **.gitignore** - Gitignore file, more info at Git_.

* **.gitlab-ci.yml** - Gitlab CI configuration, more info at `GitLab CI`_.

* **CHANGELOG.rst** - History of changes.

* **LICENSE** - License file.

* **Makefile** - Contains useful commands, more info ``make help``.

* **.pre-commit-config.yaml** - Config for the `pre-commit`_ (by Yelp) linting aggregation framework.

* **README.rst** - This file.

* **requirements.txt** - Production requirements can be also specified here.

* **setup.cfg** - Setup configuration, more info at PythonPackaging_.

* **setup.py** - Setup file, more info at PythonPackaging_.

Thanks
------

Huge thanks to all who helped to make this happen.

* My girlfriend, for being patient with me while I have been coding at home

* Remak_ - Awesome team of people determined to learn Python

* `Talk Python to me podcast`_ - for delivering huge amount of motivation and knowledge, for free!

* Thanks to `@webknjaz`_ for the review and advice

* Based on: `audreyr/cookiecutter-pypackage`_, `ionelmc/cookiecutter-pylibrary`_, `costastf/python_cli_cookiecutter`_


.. _Cookiecutter: https://cookiecutter.readthedocs.io
.. _Tox: http://testrun.org/tox/
.. _Sphinx: http://sphinx-doc.org/
.. _ReadTheDocs: https://readthedocs.org/
.. _PyTest: https://docs.pytest.org/
.. _pre-commit: http://pre-commit.com/
.. _pytest-runner: https://github.com/pytest-dev/pytest-runner
.. _setuptools_scm: https://github.com/pypa/setuptools_scm
.. _PythonPackaging: https://packaging.python.org/tutorials/distributing-packages/
.. _Make: https://www.gnu.org/software/make/manual/make.html
.. _Bash: https://www.gnu.org/software/bash/manual/bash.html
.. _Python: https://python.org
.. _Talk Python to me podcast: https://talkpython.fm
.. _Git: https://git-scm.com/book/
.. _GitLab: https://about.gitlab.com
.. _GitLab CI: https://about.gitlab.com/features/gitlab-ci-cd/
.. _Remak: http://www.remak.eu/en
.. _JSON: http://www.json.org
.. _YAML: http://yaml.org
.. _Virtualenv Wrapper: http://virtualenvwrapper.readthedocs.io
.. _Virtualenv: https://virtualenv.pypa.io
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _Pylint: https://www.pylint.org
.. _Coverage: https://coverage.readthedocs.io/
.. _@webknjaz: https://github.com/webknjaz
.. _SemVer: http://semver.org
.. _audreyr/cookiecutter-pypackage: https://github.com/audreyr/cookiecutter-pypackage
.. _ionelmc/cookiecutter-pylibrary: https://github.com/ionelmc/cookiecutter-pylibrary
.. _costastf/python_cli_cookiecutter: https://github.com/costastf/python_cli_cookiecutter
.. _setuptools-git: https://github.com/msabramo/setuptools-git
