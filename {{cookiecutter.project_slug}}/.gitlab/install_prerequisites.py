#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Parse and install rwt-based pre-requisites."""

import ast
import pip


def get_reqs(setup_filename='setup.py'):
    """Return an iterator over the requirements list.

    Check for __requires__ contents in distribution setup.
    """
    with open(setup_filename, 'r') as setup_file:
        ast_setup = ast.parse(setup_file.read(), setup_filename)

    try:
        return (r.s
                for r in next(
                    ass for ass in ast_setup.body
                    if isinstance(ass, ast.Assign) and getattr(ass.targets[0], 'id', None) == '__requires__'
                ).value.elts)
    except StopIteration:  # Handle case when there's no __requires__ entry
        return ()


def pip_install_prerequisites():
    """Install the requirements, found in distribution setup module."""
    prerequisites = get_reqs()

    if not prerequisites:
        return

    install_cmd = ['install'] + list(prerequisites)
    pip.main(install_cmd)


__name__ == '__main__' and pip_install_prerequisites()  # pylint: disable=W0106
