#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
conftest.py contain fixtures for tests of {{ cookiecutter.project_slug }}.

conftest.py
----------------------------------
We are writing tests with pytest.
See more at: http://doc.pytest.org

More info about this file: https://stackoverflow.com/questions/34466027/in-py-test-what-is-the-use-of-conftest-py-files
"""

from urllib.request import urlopen

import pytest  # pylint: disable=E0401


@pytest.fixture
def response():
    """
    Sample pytest fixture.

    See more at: http://doc.pytest.org/en/latest/fixture.html
    """
    return urlopen('https://github.com/audreyr/cookiecutter-pypackage')
