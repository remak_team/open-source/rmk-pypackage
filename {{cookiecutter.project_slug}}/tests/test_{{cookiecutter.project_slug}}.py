#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Example test suite for {{ cookiecutter.project_slug }}.

test_{{ cookiecutter.project_slug }}
----------------------------------
We are writing tests with pytest.
See more at: http://doc.pytest.org

good practices: https://docs.pytest.org/en/latest/goodpractices.html

Tests for `{{ cookiecutter.project_slug }}` module.
"""


from {{cookiecutter.project_slug}} import {{(cookiecutter.project_slug.title()).replace('_', '')}}


def test_content(response):  # pylint: disable=W0621
    """
    Sample pytest test function with the pytest fixture as an argument.

    Parameters
    ----------
    response : str(data type of the argument)
        Description of the argument

    """
    assert 'GitHub.com' in response.headers['Server']


def test_simple_add_method():
    """Sample test of static method, using pytest."""
    assert {{(cookiecutter.project_slug.title()).replace('_', '')}}.simple_add(1, 3) == 4
